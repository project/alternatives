<?php

/**
 * Form builder callback for the form on 'admin/modules/alternatives'.
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array
 *
 * @see alternatives_admin_form_submit()
 */
function alternatives_admin_form(array $form, array &$form_state) {

  /** @noinspection SuspiciousAssignmentsInspection */
  $form = array();

  $options = _alternatives_admin_form_build_options();

  if ($value = variable_get(VARNAME_ALTERNATIVES_TO_IGNORE, array())) {
    $obsolete_options = array_diff_key($value, $options);
    foreach ($obsolete_options as $module) {
      $options[$module] = $module . ' (' . t('obsolete') . ')';
    }
  }

  if (array() === $options) {
    drupal_set_message(
      t('Currently no enabled modules declare any dependencies.'));
  }

  $form[VARNAME_ALTERNATIVES_TO_IGNORE] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#title' => t('Alternatives to ignore'),
    '#description' => t('Selected modules will not be considered as alternative dependencies.'),
    '#default_value' => $value,
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));

  $form['#theme'] = 'system_settings_form';

  return $form;
}

/**
 * @return string[]
 *   Format: $[$module] = $name
 */
function _alternatives_admin_form_build_options() {

  $system_info = system_get_info('module');

  $collect = array();
  foreach ($system_info as $module => $info) {
    if (empty($info['alternatives'])) {
      continue;
    }
    if (!is_array($info['alternatives'])) {
      drupal_set_message(
        t(
          'Alternatives declared in @file are not an array.',
          array('@file' => $info->file)),
        'warning');
      continue;
    }
    foreach ($info['alternatives'] as $replaceable => $alternatives) {
      if (!is_array($alternatives)) {
        drupal_set_message(
          t(
            'Alternatives entry for @replaceable declared in @file is not an array.',
            array('@replaceable' => $replaceable, '@file' => $info->file)),
          'warning');
        continue;
      }
      foreach ($alternatives as $alternative) {
        $collect[$alternative] = TRUE;
      }
      $collect[$replaceable] = TRUE;
    }
  }

  $module_list = module_list();

  $options = array();
  foreach ($collect as $alternative => $_true) {
    if (!isset($module_list[$alternative]) || !isset($system_info[$alternative])) {
      continue;
    }
    $options[$alternative] = $system_info[$alternative]['name'];
  }

  asort($options);

  return $options;
}

/**
 * Form submit callback for 'alternatives_admin_form'.
 *
 * @param array $form
 * @param array $form_state
 */
function alternatives_admin_form_submit(array $form, array &$form_state) {
  $values = $form_state['values'];
  $setting = array_filter($values[VARNAME_ALTERNATIVES_TO_IGNORE]);
  variable_set(VARNAME_ALTERNATIVES_TO_IGNORE, $setting);

  // @todo Don't we need to clear any caches?
  // Drush seems to take the changes without clearing caches.

  drupal_set_message(t('The configuration options have been saved.'));
}
